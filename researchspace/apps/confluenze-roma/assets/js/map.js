const waitForElement = (id, callback) => {
    var isElementAlive = setInterval(function() {
        if (document.getElementById(id)) {
            clearInterval(isElementAlive);
            callback();
        }
    }, 100);
};

var map;

waitForElement("map", () => {

$("#sidebar").hide('fast');
$("#box").hide('fast');

    mapboxgl.accessToken = "pk.eyJ1Ijoia2V3ZXJuZXIiLCJhIjoiY2lqMGprNjVmMDA0NnYwbTVibHh5bWx6aSJ9.zB8-7hnUeIeYJsqzOjJ2Fg";
    if (!mapboxgl.supported()) {
        alert("Your hardware and/or software does not support Mapbox GL. Please update.");
        return
    } else {
        map = new mapboxgl.Map({
            container: "map",
            style: "mapbox://styles/kewerner/clu9v8k53000q01nt0v7yd3h1",
            //        style: "mapbox://styles/kewerner/clsvhuomz005401qw6u6xdk0w",  //new v3 version
            center: [12.48351, 41.905568],
            hash: true,
            zoom: 18.2,
            pitch: 40,
            maxPitch: 40,
            bearing: -70.6
        });
    }
    map.on("style.load", function() {
        //GNA
        map.addSource("gna_polygonz", {
            "type": "geojson",
            "data": "/assets/json/gna_multipolygon.geojson"
        });
        
        map.addSource("rome_bigdata", {
            "type": "geojson",
            "data": "https://dlib.biblhertz.it/urbs2/data/rome_bigdata.geojson"
        });
        
        
        map.addLayer({
            "id": "gna-polygon",
            "type": "fill",
            "slot": "top",
            "source": "rome_bigdata",
            "layout": {},
            "paint": {
                "fill-color": "Crimson",
                "fill-opacity": 0.15,
                'fill-outline-color': 'white',
            },
            "interactive": true
        });
        map.addLayer({
            'id': 'label-gna-polygon',
            'type': 'symbol',
            'source': 'rome_bigdata',
            'layout': {
                'icon-image': 'tw-provincial-expy-3',
                'icon-size': 0.5,
                'text-field': ['get', 'ogd'],
                'text-font': ['Overpass Regular'],
                'text-variable-anchor': ['bottom'],
                'text-radial-offset': 1.5,
                'text-justify': 'auto',
                'text-size': 13
            },
            'paint': {
                'text-color': 'darkred',
                'text-halo-width': 2.5,
                'text-halo-blur': 0.5,
                'text-halo-color': 'lightgrey'
            },
            "interactive": true
        });
        map.addSource("gna_point", {
            "type": "geojson",
            "data": "/assets/json/gna_multipoint.geojson"
        });
        map.addLayer({
            'id': 'gna-point',
            'type': 'symbol',
            'source': 'rome_bigdata',
            'layout': {
                'icon-image': 'tw-provincial-expy-3',
                'icon-size': 0.5,
                'text-field': ['get', 'ogd'],
                'text-font': ['Overpass Regular'],
                'text-variable-anchor': ['bottom'],
                'text-radial-offset': 1.5,
                'text-justify': 'auto',
                'text-size': 13
            },
            'paint': {
                'text-color': 'darkred',
                'text-halo-width': 2.5,
                'text-halo-blur': 0.5,
                'text-halo-color': 'lightgrey'
            },
            "interactive": true
        });

        //Wikidata
        map.addSource("wikidata", {
            "type": "geojson",
            "data": "/assets/json/wdosm.geojson"
        });

        map.addLayer({
            "id": "wd-polygon",
            "type": "fill-extrusion",
            "slot": "top",
            "source": "rome_bigdata",
            "layout": {},
            "paint": {
                "fill-extrusion-height": 10,
                // 'fill-extrusion-color': 'CornflowerBlue',
                "fill-extrusion-opacity": 0
            },
            'filter': ['==', '$type', 'Polygon'],
            "interactive": true
        });
        map.addLayer({
            "id": "wd-line",
            "type": "fill-extrusion",
            "source": "rome_bigdata",
            "layout": {},
            "paint": {
                "fill-extrusion-height": 10,
                // 'fill-extrusion-color': 'crimson',
                "fill-extrusion-opacity": 0
            },
            'filter': ['==', '$type', 'LineString'],
            "interactive": true
        });
        map.addLayer({
            'id': 'wd-point',
            'type': 'circle',
            'source': 'rome_bigdata',
            'paint': {
                'circle-radius': 6,
                // 'circle-color': 'crimson',
                'circle-opacity': 0
            },
            'filter': ['==', '$type', 'Point'],
            "interactive": true
        });
        map.addLayer({
            'id': 'wd-label',
            'type': 'symbol',
            'source': 'rome_bigdata',
            'layout': {
                'icon-image': 'tw-provincial-2',
                //            'icon-image': 'leader_line',
                //            'text-offset': [0, -12.5],
                'icon-size': 0.5,
                'text-field': ['get', 'label'],
                'text-font': ['Overpass Regular'],
                'text-variable-anchor': ['bottom'],
                'text-radial-offset': 1,
                'text-justify': 'auto',
                'text-size': 13
            },
            'paint': {
                'text-color': 'hsl(210, 60%, 38%)',
                'text-halo-width': 2.5,
                'text-halo-blur': 0.5,
                'text-halo-color': 'lightgrey'
            },
            "interactive": true
        });
    });

    $("#nav button").hide(); //#osm


    map.on("click", function(e) {
    

    
    
        var features = map.queryRenderedFeatures(e.point, {
            layers: ['gna-point', 'gna-polygon', 'wd-polygon', 'wd-line', 'wd-point']
        });


        if (!features.length) {
            $("#sidebar, #box").hide('slow');
            $("#nav button").hide(); //#osm, 
            return;
        }
        if (features.length) {

            $("#nav").on('click', '#bhmpibib', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://aleph.mpg.de/F/?func=find-a&find_code=PER&request=&request_op=AND&find_code=WTI&request=&request_op=AND&find_code=WKO&request=&request_op=AND&filter_code_1=WSP&filter_request_1=&filter_code_2=WYR&filter_request_2=&filter_code_3=WYR&filter_request_3=&filter_code_4=WEF&filter_request_4=&local_base=KUB01&filter_code_7=WEM&filter_code_8=WAK&find_code=IGD&request=' + features[0].properties.gnd + '" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
                // }).on('click', '#osm', function(){
                //   $("iframe").remove();
                //   $("#box").show('slow');
                //   document.getElementById("box").innerHTML = '<iframe src="https://www.openstreetmap.org/' + features[0].properties.osm + '/?layers=D" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                //   return;
            }).on('click', '#iiif', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://dlib.biblhertz.it/m3s/?show=' + features[0].properties.iiif + '" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#commons', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://commons.wikimedia.org/wiki/Category:' + features[0].properties.commons + '?useskin=timeless" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#wikipedia', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://it.wikipedia.org/wiki/' + features[0].properties.wikipedia + '?useskin=timeless" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#beweb', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://chieseitaliane.chiesacattolica.it/chieseitaliane/AccessoEsterno.do?mode=guest&type=auto&code=' + features[0].properties.beweb + '" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#iccds', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://dati.beniculturali.it/lodview/iccd/schede/resource/uod/S0' + features[0].properties.iccds + '.html" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#iccdch', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://catalogo.beniculturali.it/detail/' + features[0].properties.iccdch + '" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#dbunico', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://dati.beniculturali.it/mibact/luoghi/resource/CulturalInstituteOrSite/' + features[0].properties.dbunico + '" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#culturaitalia', function() {
                $("iframe").remove();
                $("#box").show('slow');
                url = features[0].properties.culturaitalia;
                encodedUrl = encodeURI(url);
                document.getElementById("box").innerHTML = '<iframe src="https://www.culturaitalia.it/opencms/museid/viewItem.jsp?id=' + encodedUrl + '" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#wikidata', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://sqid.toolforge.org/#/view?id=' + features[0].properties.wikidata + '&lang=it" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#architect', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://angryloki.github.io/wikidata-graph-builder/?item=' + features[0].properties.architect + '&property=P800&mode=undirected&lang=it&graph_direction=left" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            }).on('click', '#haspart', function() {
                $("iframe").remove();
                $("#box").show('slow');
                document.getElementById("box").innerHTML = '<iframe src="https://angryloki.github.io/wikidata-graph-builder/?item=' + features[0].properties.haspart + '&property=P170&mode=undirected&lang=it&graph_direction=left" title="iFrame" width="100%" height="100%" scrolling="yes" frameborder="no" allow=""></iframe>';
                return;
            });

            // reset the box layer:

            $("#box").hide('slow');
            $("#architect, #haspart, #wikidata, #wikipedia, #bhmpibib, #iiif, #commons, #iccdch, #iccds, #dbunico, #beweb").hide(); //#osm


            $("#sidebar").show('slow');
            if (features[0].properties.name) {
                document.getElementById("sidebar").innerHTML = '<span class="name">' + features[0].properties.name + '</span><br/><br/>';
            } else if (features[0].properties.accc) {
                document.getElementById("sidebar").innerHTML = '<span class="name">' + features[0].properties.ogt + '</span><br/><br/>';
            } else {
                document.getElementById("sidebar").innerHTML = '<span class="name">[' + features[0].properties.amenity + ']</span><br/><br/>';
            }
            document.getElementById("sidebar").innerHTML += '<table><tbody id="tcontainer"/></table>';
            if (features[0].properties.osm) {
                document.getElementById("tcontainer").innerHTML += '<tr><td>OSM</td><td><a class="link" target="data" href="https://www.openstreetmap.org/' + features[0].properties.osm + '">' + features[0].properties.osm + ' â‡—</a></td></tr>';
            }
            if (features[0].properties.architect) {
                $("#architect").show('slow');
            }
            if (features[0].properties.haspart) {
                $("#haspart").show('slow');
            }
            if (features[0].properties.gnd) {
                $("#bhmpibib").show('slow');
            }
            if (features[0].properties.fototecaobj) {}
            if (features[0].properties.iiif) {
                $("#iiif").show('slow');
            }
            if (features[0].properties.commons) {
                $("#commons").show('slow');
            }
            // if (features[0].properties.osm) {
            //     $("#osm").show('slow');
            // }
            if (features[0].properties.beweb) {
                $("#beweb").show('slow');
            }
            if (features[0].properties.iccds) {
                $("#iccds").show('slow');
            }
            if (features[0].properties.iccdch) {
                $("#iccdch").show('slow');
            }
            if (features[0].properties.dbunico) {
                $("#dbunico").show('slow');
            }
            if (features[0].properties.culturaitalia) {
                $("#culturaitalia").show('slow');
            }
            if (features[0].properties.wikidata) {
                $("#wikidata").show('slow');
                
                       function httpGetAsync(theUrl) {
            $.ajax({
                url: theUrl,
                data: {
                    format: 'json'
                },
                error: function() {
                    console.log("error");
                },
                dataType: 'json',
                success: function(data) {
                    console.log(JSON.stringify(data));
                    var link = data.results.bindings[0].x.value;
                    $("a").attr("href", "/resource/?uri="+encodeURIComponent(link))
                },
                type: 'GET'
            });
        }
        //sparql query
        var query = [
            "PREFIX owl: <http://www.w3.org/2002/07/owl#>",
            "SELECT DISTINCT * WHERE { ?x (owl:sameAs|^owl:sameAs)+",
            "<http://www.wikidata.org/entity/"+features[0].properties.wikidata+">",
            " }"
        ].join(" ");
        //url for the query
        console.log(query);
        var url = "http://localhost:3030/confluenze/sparql";
        var queryUrl = url + "?query=" + encodeURIComponent(query);
        //query call
        httpGetAsync(queryUrl);
                
                
            }
            if (features[0].properties.wikipedia) {
                $("#wikipedia").show('slow');
            }
            if (features[0].properties.accc) {
                document.getElementById("tcontainer").innerHTML += '<tr><td colspan="2"><hr/>Estratto Geoportale Nazionale per lâ€™Archeologia<br/><hr/></td></tr><tr><td>GNA ID</td><td><a class="link" target="data" href="https://gna.cultura.gov.it/mappa.html">' + features[0].properties.accc + ' â‡—</a></td></tr><tr><td>Definizione</td><td>' + features[0].properties.ogd + '</td></tr><tr><td>Descrizione</td><td>' + features[0].properties.des + '</td></tr><tr><td>Datazione</td><td>' + features[0].properties.dtr + '</td></tr><tr><td>Bibliografia</td><td>' + features[0].properties.bibm + '</td></tr><tr><td>Inserito</td><td>' + features[0].properties.data_ins.substr(0, 10) + '</td></tr><tr><td>Aggiornato</td><td>' + features[0].properties.data_upd.substr(0, 10) + '</td></tr>';
            }
            if (features[0].properties.inception) {
                document.getElementById("tcontainer").innerHTML += '<tr><td>Anno di costruzione</td><td>AD ' + features[0].properties.inception.substr(0, 4) + '</td></tr>';
            }
            if (features[0].properties.img) {
                document.getElementById("sidebar").innerHTML += '<div style="margin-top: 2rem; margin-bottom: 2rem; padding: 1.5rem; text-align: center; background-color: rgb(245,245,245); border: 4px solid white; border-radius: 8px;"><img style=" max-width: 20rem; max-height: 20rem;" src="' + features[0].properties.img + '"/></div>'
            }

            //start getting live wikipedia
            if (features[0].properties.wikipedia) {
                var xhr = new XMLHttpRequest();
                var url = 'https://it.wikipedia.org//w/api.php?action=query&format=json&origin=*&prop=revisions&formatversion=2&prop=extracts&titles=' + features[0].properties.wikipedia; //versione stesa
                // var url = 'https://it.wikipedia.org/w/api.php?format=json&origin=*&action=query&prop=extracts&explaintext=false&exintro&titles=' + features[0].properties.wikipedia;     //version compatta
                // var url = "https://en.wikipedia.org/w/api.php?format=json&origin=*&action=query&prop=extracts&explaintext=false&exintro&titles=Kobe+Bryant";   //test
                xhr.open('GET', url, true);
                xhr.onload = function() {
                    // Parse the request into JSON
                    var data = JSON.parse(this.response);
                    // console.log(data);
                    // console.log(data.query.pages)
                    for (var i in data.query.pages) {
                        // console.log(data.query.pages[i].extract);
                        var title = data.query.pages[i].title;
                        var text = data.query.pages[i].extract;
                        //                    document.getElementById("wpedia").className = 'shown';
                        document.getElementById("sidebar").innerHTML += '<div id="wpedia"><hr/>Estratto Wikipedia<br/><hr/><h1>' + title + '</h1><div>' + text + '</div><div>Licenza <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.it">CC BY-SA</a></div></div>';
                    }
                }
                xhr.send();
            }
            //end live wikipedia

        } else {
            //if not hovering over a feature -- remove, unncessary because of line 230 "if (!features.length)"
            $("#sidebar").hide('fast');
            $("#box").hide('slow');
        }
    });

    //adding some controls
    map.addControl(new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl
    }));


    //map.addControl(new mapboxgl.FullscreenControl());
    map.addControl(new mapboxgl.FullscreenControl({
        container: document.querySelector('body')
    }));
    map.addControl(new mapboxgl.NavigationControl());

    map.addControl(
        new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true,
            showUserHeading: true
        })
    );
    


});
