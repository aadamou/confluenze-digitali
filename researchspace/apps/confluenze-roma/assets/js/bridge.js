// remove fragment as much as it can go without adding an entry in browser history:
window.location.replace("#");

// slice off the remaining '#' in HTML5:    
if (typeof window.history.replaceState == 'function') {
  history.replaceState({}, '', window.location.href.slice(0, -1));
}

function jumpTo(index){
    map.flyTo({
    center: index,
    zoom: 18,
    speed: 2,
    curve: 0.4,
    easing(t) {
        return t;
    }
});
}

$(document).on('click',".direct a#seeonmap",function(e){
    e.preventDefault();
    var coo = /\((\d*\.?\d*)\s+(\d*\.?\d*)\)/.exec($(this).data('coord'))
    jumpTo([coo[1],coo[2]])
})
